<!DOCTYPE HTML>
<html lang="pl">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85041758-2', 'auto');
  ga('send', 'pageview');

</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Formularz kontaktowy wysłany poprawnie</title>
<meta name="description" content="Tworzymy strony internetowe dla firm i osób prywatnych. Gwarantujemy indywidualne podejście do każdego projektu i najniższe ceny na rynku!" />
<meta name= "keywords" content="strony internetowe,strony internetowe dla firm,websiteworkshop,website for my company,tworzenie stron internetowych,strony internetowe wrocław,tworzenie stron internetowych wrocław,strony internetowe kłodzko,najtansze strony internetowe"/>
<meta name="application-name" content="websiteworkshop.pl" />
<meta name="author" content="Wojciech Gąsiewicz" />
<meta name="distributor" content="" />
<meta name="robots" content="All" />
<meta name="dcterms.contributor" content="Wojciech Gąsiewicz" />
<meta name="dcterms.creator" content="Wojciech Gąsiewicz" />
<meta name="dcterms.publisher" content="Wojciech Gąsiewicz" />
<meta name="dcterms.description" content="Tworzymy strony internetowe dla firm i osób prywatnych. Gwarantujemy indywidualne podejście do każdego projektu i najniższe ceny na rynku!" />
<meta name="dcterms.rights" content="" />
<meta property="og:type" content="website" />
<meta property="og:title" content="websiteworkshop.pl" />
<meta property="og:description" content="Tworzymy strony internetowe dla firm i nie osób prywatnych. Gwarantujemy indywidualne podejście do każdego projektu i najniższe ceny na rynku!" />
<meta property="twitter:title" content="websiteworkshop.pl" />
<meta property="twitter:description" content="Tworzymy strony internetowe dla firm i osób prywatnych. Gwarantujemy indywidualne podejście do każdego projektu i najniższe ceny na rynku!" />
<link rel="stylesheet" type="text/css" href="formstyle.css" />
</head>

<body>

<div id="formularz">
<p>Twoja wiadomość została pomyślnie wysłana.</p>
<a href="http://websiteworkshop.pl"><p>Powrót do strony.</p></a>
</div>

</body>

</html>