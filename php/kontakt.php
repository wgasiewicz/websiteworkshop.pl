<?php
$name_error = $email_error ="";
// Tworzymy zmienną dla imienia i nazwiska
$name = $_POST['name'];

// Tworzymy zmienną dla adresu email
$email = $_POST['email'];

// Tworzymy zmienną dla wiadomości
$message = $_POST['message'];

// Podajesz adres email z którego ma być wysłana wiadomość
$odkogo = "oferta@websiteworkshop.pl";

// Podajesz adres email na który chcesz otrzymać wiadomość
$dokogo = "biuro@websiteworkshop.pl";

// Podajesz tytuł jaki ma mieć ta wiadomość email
$tytul = "Kontakt w sprawie strony www";

// Przygotowujesz treść wiadomości
$wiadomosc = "";
$wiadomosc .= "Imie i nazwisko: " . $name . "\n";
$wiadomosc .= "Email: " . $email . "\n";
$wiadomosc .= "Wiadomość: " . $message . "\n";

$naglowek = "";
$naglowek .= "Od:" . $odkogo . " \n";
$naglowek .= "Content-Type:text/plain;charset=utf-8";

// Wysyłamy wiadomość


// Przekierowywujemy na potwierdzenie
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST['name'])) {
    $name_error = "Imię jest wymagane!";
  } else {
    $name = test_input($_POST['name']);
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $name_error = "Tylko litery i spacje!"; 
    }
  }

  if (empty($_POST['email'])) {
    $email_error = "Email jest wymagany!";
  } else {
    $email = test_input($_POST['email']);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email_error = "Błędny adres email!"; 
    }
  }
 
 if($name_error=='' and $email_error=='')
 {
	 print "<meta http-equiv=\"refresh\" content=\"0;URL=potwierdzenie.php\">";
	 $message=$email=$name='';
	 $sukces = mail($dokogo, $tytul, $wiadomosc, $naglowek);
 }
 else{print "<meta http-equiv=\"refresh\" content=\"0;URL=error.html\">";}
}
//if ($sukces){
//  print "<meta http-equiv=\"refresh\" content=\"0;URL=potwierdzenie.php\">";
//}
//else{
//  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.html\">";
//}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>