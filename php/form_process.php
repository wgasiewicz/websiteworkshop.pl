<?php 
$name_error = $email_error = $phone_error = "";
$name = $email = $phone = $message  = $success = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $name_error = "Imię jest wymagane!";
  } else {
    $name = test_input($_POST["name"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $name_error = "Tylko litery i spacje!"; 
    }
  }

  if (empty($_POST["email"])) {
    $email_error = "Email jest wymagany!";
  } else {
    $email = test_input($_POST["email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email_error = "Błędny adres email!"; 
    }
  }
  
  if (empty($_POST["phone"])) {
    $phone_error = "Telefon jest wymagany!";
  } else {
    $phone = test_input($_POST["phone"]);
    if (!preg_match("/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i",$phone)) {
      $phone_error = "Błędny numer telefonu!"; 
    }
  }

 print "<meta http-equiv=\"refresh\" content=\"0;URL=potwierdzenie.php\">";
  if (empty($_POST["message"])) {
    $message = "";
  } else {
    $message = test_input($_POST["message"]);
  }
  
  if ($name_error == '' and $email_error == '' and $phone_error == '' ){
      $message_body = '';
      unset($_POST['submit']);
      foreach ($_POST as $key => $value){
          $message_body .=  "$key: $value\n";
      }
      
      $to = 'biuro@websiteworkshop.pl';
      $subject = 'Kontakt w sprawie strony www';
      if (mail($to, $subject, $message_body))﻿{
          $success = "Wiadomość wysłana!";
          $name = $email = $phone = $message = '';
		   
      }
  }
  
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}